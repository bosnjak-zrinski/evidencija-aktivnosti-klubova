import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Igrac } from './igrac';

@Component({
  selector: 'app-igrac',
  templateUrl: './igrac.component.html',
  styleUrls: ['./igrac.component.css']
})
export class IgracComponent {

  @Input() igrac: Igrac | null = null;
  @Output() edit = new EventEmitter<Igrac>();

}

