export interface Igrac {
    id?: string;
    ime: string;
    prezime: string;
    god_rodenja: number;
    br_nastupa?: number;
    br_golova: number;
  }