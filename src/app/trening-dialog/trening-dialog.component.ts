import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Trening } from '../trening/trening';

@Component({
  selector: 'app-trening-dialog',
  templateUrl: './trening-dialog.component.html',
  styleUrls: ['./trening-dialog.component.css']
})
export class TreningDialogComponent {

  private backupTrening: Partial<Trening> = { ...this.data.trening };

  constructor(
    public dialogRef: MatDialogRef<TreningDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TreningDialogData
  ) { }

  cancel(): void {
    this.data.trening.datum_tr = this.backupTrening.datum_tr;
    this.data.trening.naziv_tr = this.backupTrening.naziv_tr;
    this.data.trening.opis_tr = this.backupTrening.opis_tr;
    this.dialogRef.close(this.data);
  }
}
  
export interface TreningDialogData {
  trening: Partial<Trening>;
  enableDelete: boolean;
}

export interface TreningDialogResult {
  trening: Trening;
  delete?: boolean;
}