import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TreningDialogComponent } from './trening-dialog.component';

describe('TreningDialogComponent', () => {
  let component: TreningDialogComponent;
  let fixture: ComponentFixture<TreningDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TreningDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TreningDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
