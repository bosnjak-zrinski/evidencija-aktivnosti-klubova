import { Component } from '@angular/core';
import { Task } from './task/task';
import { Trening } from './trening/trening';
import { Igrac } from './igrac/igrac';
import { MatDialog } from '@angular/material/dialog';
import {
  TaskDialogComponent,
  TaskDialogResult,
} from './task-dialog/task-dialog.component';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import {
  TreningDialogComponent,
  TreningDialogResult,
} from './trening-dialog/trening-dialog.component';
import {
  IgracDialogComponent,
  IgracDialogResult,
} from './igrac-dialog/igrac-dialog.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  igraciKolekcija: AngularFirestoreCollection<Igrac>;
  treninziKolekcija: AngularFirestoreCollection<Trening>;
  utakmiceKolekcija: AngularFirestoreCollection<Task>;

  igraci: Observable<Igrac[]>;
  treninzi: Observable<Trening[]>;
  utakmice: Observable<Task[]>;

  constructor(private dialog: MatDialog, private store: AngularFirestore) {
    this.igraciKolekcija = this.store.collection('igraci');
    this.treninziKolekcija = this.store.collection('treninzi');
    this.utakmiceKolekcija = this.store.collection('utakmice');

    this.igraci = this.igraciKolekcija.valueChanges({ idField: 'id' });
    this.treninzi = this.treninziKolekcija.valueChanges({ idField: 'id' });
    this.utakmice = this.utakmiceKolekcija.valueChanges({ idField: 'id' });
  }

  editUtakmica(list: 'utakmice', task: Task): void {
    const dialogRef = this.dialog.open(TaskDialogComponent, {
      width: '270px',
      data: {
        task,
        enableDelete: true,
      },
    });
    dialogRef.afterClosed().subscribe((result: TaskDialogResult) => {
      if (result.delete) {
        this.store.collection(list).doc(task.id).delete();
      } else {
        this.store.collection(list).doc(task.id).update(task);
      }
    });
  }

  editTrening(list: 'treninzi', trening: Trening): void {
    const dialogRef = this.dialog.open(TreningDialogComponent, {
      width: '270px',
      data: {
        trening,
        enableDelete: true,
      },
    });
    dialogRef.afterClosed().subscribe((result: TreningDialogResult) => {
      if (result.delete) {
        this.store.collection(list).doc(trening.id).delete();
      } else {
        this.store.collection(list).doc(trening.id).update(trening);
      }
    });
  }

  editIgrac(list: 'igraci', igrac: Igrac): void {
    const dialogRef = this.dialog.open(IgracDialogComponent, {
      width: '270px',
      data: {
        igrac,
        enableDelete: true,
      },
    });
    dialogRef.afterClosed().subscribe((result: IgracDialogResult) => {
      if (result.delete) {
        this.store.collection(list).doc(igrac.id).delete();
      } else {
        this.store.collection(list).doc(igrac.id).update(igrac);
      }
    });
  }

  novaUtakmica(): void {
    const dialogRef = this.dialog.open(TaskDialogComponent, {
      width: '270px',
      data: {
        task: {},
      },
    });
    dialogRef
      .afterClosed()
      .subscribe((result: TaskDialogResult) =>
        this.store.collection('utakmice').add(result.task)
      );
  }

  noviTrening(): void {
    const dialogRef = this.dialog.open(TreningDialogComponent, {
      width: '270px',
      data: {
        trening: {},
      },
    });
    dialogRef
      .afterClosed()
      .subscribe((result: TreningDialogResult) =>
        this.store.collection('treninzi').add(result.trening)
      );
  }

  noviIgrac(): void {
    const dialogRef = this.dialog.open(IgracDialogComponent, {
      width: '270px',
      data: {
        igrac: {},
      },
    });
    dialogRef
      .afterClosed()
      .subscribe((result: IgracDialogResult) =>
        this.store.collection('igraci').add(result.igrac)
      );
  }

    key: string = 'id';
    reverse: boolean = false;
    sort(key: string) {
      this.key = key;
      this.reverse = !this.reverse;
    }

    key2: string = 'id';
    reverse2: boolean = false;
    sort2(key2: string) {
      this.key2 = key2;
      this.reverse2 = !this.reverse2;
    }

    key3: string = 'id';
    reverse3: boolean = false;
    sort3(key3: string) {
      this.key3 = key3;
      this.reverse3 = !this.reverse3;
    }
    
}

