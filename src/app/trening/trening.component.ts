import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Trening } from './trening';

@Component({
  selector: 'app-trening',
  templateUrl: './trening.component.html',
  styleUrls: ['./trening.component.css']
})
export class TreningComponent {

  @Input() trening: Trening | null = null;
  @Output() edit = new EventEmitter<Trening>();

}
