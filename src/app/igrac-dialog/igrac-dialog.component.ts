import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Igrac } from '../igrac/igrac';

@Component({
  selector: 'app-igrac-dialog',
  templateUrl: './igrac-dialog.component.html',
  styleUrls: ['./igrac-dialog.component.css']
})
export class IgracDialogComponent {
  private backupIgrac: Partial<Igrac> = { ...this.data.igrac };

  constructor(
    public dialogRef: MatDialogRef<IgracDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IgracDialogData
  ) { }

  cancel(): void {
    this.data.igrac.ime = this.backupIgrac.ime;
    this.data.igrac.prezime = this.backupIgrac.prezime;
    this.data.igrac.god_rodenja = this.backupIgrac.god_rodenja;
    this.data.igrac.br_nastupa = this.backupIgrac.br_nastupa;
    this.data.igrac.br_golova = this.backupIgrac.br_golova;
    this.dialogRef.close(this.data);
  }

}

export interface IgracDialogData {
  igrac: Partial<Igrac>;
  enableDelete: boolean;
}

export interface IgracDialogResult {
  igrac: Igrac;
  delete?: boolean;
}
