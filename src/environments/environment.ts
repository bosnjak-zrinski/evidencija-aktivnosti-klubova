// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBCdKkQprhREKmI5F5LodDY6TtPnhnGDlY',
    authDomain: 'aplikacija-a80f5.firebaseapp.com',
    projectId: 'aplikacija-a80f5',
    storageBucket: 'aplikacija-a80f5.appspot.com',
    messagingSenderId: '273236276591',
    appId: '1:273236276591:web:d1d15377451bdb34f7e49a',
    measurementId: 'G-EMNPCD681E',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
